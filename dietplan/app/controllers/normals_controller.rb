class NormalsController < ApplicationController
  before_action :set_normal, only: [:show, :edit, :update, :destroy]

  # GET /normals
  # GET /normals.json
  def index
    @normals = Normal.all
  end

  # GET /normals/1
  # GET /normals/1.json
  def show
  end

  # GET /normals/new
  def new
    @normal = Normal.new
  end

  # GET /normals/1/edit
  def edit
  end

  # POST /normals
  # POST /normals.json
  def create
    @normal = Normal.new(normal_params)

    respond_to do |format|

      if (@normal.present? && @normal.height.present? && @normal.weight.present? && @normal.age.present?)       
        if params["theme"] == "male"
          @normal.total = 10* @normal.weight + 6.25 * @normal.height - 5 * @normal.age + 5
        elsif params["theme"] == "female"
          @normal.total = 10* @normal.weight + 6.25 * @normal.height - 5 * @normal.age - 161        
        else
          @error = "Need to choose gender"
        end
      else
        @error = "Need to fill weight, height, and age"
    end

    if @error.present?
      format.html { render :new }   
      format.json { render json: @normal.errors, status: :unprocessable_entity }
  else

      if @normal.save
        format.html { redirect_to @normal, notice: 'Normal was successfully created.' }
        format.json { render :show, status: :created, location: @normal }
      else
        format.html { render :new }
        format.json { render json: @normal.errors, status: :unprocessable_entity }
      end
    end
  end
end

  # PATCH/PUT /normals/1
  # PATCH/PUT /normals/1.json
  def update
    respond_to do |format|
      if @normal.update(normal_params)
        format.html { redirect_to @normal, notice: 'Normal was successfully updated.' }
        format.json { render :show, status: :ok, location: @normal }
      else
        format.html { render :edit }
        format.json { render json: @normal.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /normals/1
  # DELETE /normals/1.json
  def destroy
    @normal.destroy
    respond_to do |format|
      format.html { redirect_to normals_url, notice: 'Normal was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_normal
      @normal = Normal.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def normal_params
      params.require(:normal).permit(:weight, :height, :age, :total)
    end
end
