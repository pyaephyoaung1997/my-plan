class BmitablesController < ApplicationController
  before_action :set_bmitable, only: [:show, :edit, :update, :destroy]

  # GET /bmitables
  # GET /bmitables.json
  def index
    @bmitables = Bmitable.all
  end

  # GET /bmitables/1
  # GET /bmitables/1.json
  def show
  end

  # GET /bmitables/new
  def new
    @bmitable = Bmitable.new
  end

  # GET /bmitables/1/edit
  def edit
  end

  # POST /bmitables
  # POST /bmitables.json
  def create
    @bmitable = Bmitable.new(bmitable_params)

    respond_to do |format|
      if @bmitable.save
        format.html { redirect_to @bmitable, notice: 'Bmitable was successfully created.' }
        format.json { render :show, status: :created, location: @bmitable }
      else
        format.html { render :new }
        format.json { render json: @bmitable.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bmitables/1
  # PATCH/PUT /bmitables/1.json
  def update
    respond_to do |format|
      if @bmitable.update(bmitable_params)
        format.html { redirect_to @bmitable, notice: 'Bmitable was successfully updated.' }
        format.json { render :show, status: :ok, location: @bmitable }
      else
        format.html { render :edit }
        format.json { render json: @bmitable.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bmitables/1
  # DELETE /bmitables/1.json
  def destroy
    @bmitable.destroy
    respond_to do |format|
      format.html { redirect_to bmitables_url, notice: 'Bmitable was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bmitable
      @bmitable = Bmitable.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bmitable_params
      params.require(:bmitable).permit(:bmino, :weightclass)
    end
end
