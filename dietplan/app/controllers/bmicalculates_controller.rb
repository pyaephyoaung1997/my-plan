class BmicalculatesController < ApplicationController
  before_action :set_bmicalculate, only: [:show, :edit, :update, :destroy]

  # GET /bmicalculates
  # GET /bmicalculates.json
  def index
    @bmicalculates = Bmicalculate.all
  end

  # GET /bmicalculates/1
  # GET /bmicalculates/1.json
  def show
  end

  # GET /bmicalculates/new
  def new
    @bmicalculate = Bmicalculate.new
  end

  # GET /bmicalculates/1/edit
  def edit
  end

  # POST /bmicalculates
  # POST /bmicalculates.json
  def create
    @bmicalculate = Bmicalculate.new(bmicalculate_params)

    respond_to do |format|

      if (@bmicalculate.present? && @bmicalculate.height.present? && @bmicalculate.weight.present?)

      @bmicalculate.bmi=(@bmicalculate.weight/(@bmicalculate.height*@bmicalculate.height))*703

      end

      if (@bmicalculate.present? && @bmicalculate.bmi.present? && @bmicalculate.bmi < 18.5 )
        @bmicalculate.output = 'You are underweight'
      elsif (@bmicalculate.present? && @bmicalculate.bmi.present? && @bmicalculate.bmi < 24.9 )
        @bmicalculate.output = 'You are normal'
        elsif ( @bmicalculate.present? && @bmicalculate.bmi.present? && @bmicalculate.bmi < 29.9  )
          @bmicalculate.output = 'You are overweight'
        else
          @bmicalculate.output = 'You are very overweight ( Obese )'
      end

      if @bmicalculate.save
        format.html { redirect_to @bmicalculate, notice: 'Bmicalculate was successfully created.' }
        format.json { render :show, status: :created, location: @bmicalculate }
      else
        format.html { render :new }
        format.json { render json: @bmicalculate.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /bmicalculates/1
  # PATCH/PUT /bmicalculates/1.json
  def update
    respond_to do |format|
      if @bmicalculate.update(bmicalculate_params)
        format.html { redirect_to @bmicalculate, notice: 'Bmicalculate was successfully updated.' }
        format.json { render :show, status: :ok, location: @bmicalculate }
      else
        format.html { render :edit }
        format.json { render json: @bmicalculate.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /bmicalculates/1
  # DELETE /bmicalculates/1.json
  def destroy
    @bmicalculate.destroy
    respond_to do |format|
      format.html { redirect_to bmicalculates_url, notice: 'Bmicalculate was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_bmicalculate
      @bmicalculate = Bmicalculate.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def bmicalculate_params
      params.require(:bmicalculate).permit(:weight, :height, :bmi)
    end
end