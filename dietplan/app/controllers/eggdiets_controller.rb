class EggdietsController < ApplicationController
  before_action :set_eggdiet, only: [:show, :edit, :update, :destroy]

  # GET /eggdiets
  # GET /eggdiets.json
  def index
    @eggdiets = Eggdiet.all
  end

  # GET /eggdiets/1
  # GET /eggdiets/1.json
  def show
  end

  # GET /eggdiets/new
  def new
    @eggdiet = Eggdiet.new
  end

  # GET /eggdiets/1/edit
  def edit
  end

  # POST /eggdiets
  # POST /eggdiets.json
  def create
    @eggdiet = Eggdiet.new(eggdiet_params)

    respond_to do |format|
      if @eggdiet.save
        format.html { redirect_to @eggdiet, notice: 'Eggdiet was successfully created.' }
        format.json { render :show, status: :created, location: @eggdiet }
      else
        format.html { render :new }
        format.json { render json: @eggdiet.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /eggdiets/1
  # PATCH/PUT /eggdiets/1.json
  def update
    respond_to do |format|
      if @eggdiet.update(eggdiet_params)
        format.html { redirect_to @eggdiet, notice: 'Eggdiet was successfully updated.' }
        format.json { render :show, status: :ok, location: @eggdiet }
      else
        format.html { render :edit }
        format.json { render json: @eggdiet.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /eggdiets/1
  # DELETE /eggdiets/1.json
  def destroy
    @eggdiet.destroy
    respond_to do |format|
      format.html { redirect_to eggdiets_url, notice: 'Eggdiet was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_eggdiet
      @eggdiet = Eggdiet.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def eggdiet_params
      params.require(:eggdiet).permit(:day, :breakfast, :lunch, :dinner)
    end
end
