json.extract! normal, :id, :weight, :height, :age, :total, :created_at, :updated_at
json.url normal_url(normal, format: :json)
