json.extract! thirteen, :id, :day, :breakfast, :lunch, :dinner, :created_at, :updated_at
json.url thirteen_url(thirteen, format: :json)
