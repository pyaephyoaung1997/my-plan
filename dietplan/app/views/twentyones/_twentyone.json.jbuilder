json.extract! twentyone, :id, :day, :breakfast, :morning, :lunch, :snack, :dinner, :night, :string, :created_at, :updated_at
json.url twentyone_url(twentyone, format: :json)
