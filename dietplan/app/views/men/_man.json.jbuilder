json.extract! man, :id, :height, :range, :mean, :created_at, :updated_at
json.url man_url(man, format: :json)
