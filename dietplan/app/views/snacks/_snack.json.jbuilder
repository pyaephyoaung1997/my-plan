json.extract! snack, :id, :name, :calorie, :created_at, :updated_at
json.url snack_url(snack, format: :json)
