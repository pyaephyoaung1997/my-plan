json.extract! fruit, :id, :name, :calorie, :carbohydrate, :protein, :fat, :image, :created_at, :updated_at
json.url fruit_url(fruit, format: :json)
