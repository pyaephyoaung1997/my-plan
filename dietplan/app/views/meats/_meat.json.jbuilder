json.extract! meat, :id, :name, :calorie, :carbohydrate, :protein, :fat, :image, :created_at, :updated_at
json.url meat_url(meat, format: :json)
