json.extract! bmicalculate, :id, :weight, :height, :bmi, :created_at, :updated_at
json.url bmicalculate_url(bmicalculate, format: :json)
