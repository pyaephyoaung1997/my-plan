json.extract! three, :id, :day, :breakfast, :lunch, :dinner, :created_at, :updated_at
json.url three_url(three, format: :json)
