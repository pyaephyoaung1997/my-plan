json.extract! seven, :id, :day, :breakfast, :lunch, :dinner, :created_at, :updated_at
json.url seven_url(seven, format: :json)
