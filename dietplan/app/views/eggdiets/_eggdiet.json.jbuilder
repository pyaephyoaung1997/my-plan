json.extract! eggdiet, :id, :day, :breakfast, :lunch, :dinner, :created_at, :updated_at
json.url eggdiet_url(eggdiet, format: :json)
