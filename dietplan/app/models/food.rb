class Food < ApplicationRecord
  
    validates :name, :calorie, :protein, :carbohydrate, :fat, :image, presence: true
    has_one_attached :image  
                    
end
