class Normal < ApplicationRecord
    validates :weight, :height, :age, presence: true
end
