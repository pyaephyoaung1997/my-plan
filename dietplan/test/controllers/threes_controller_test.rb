require 'test_helper'

class ThreesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @three = threes(:one)
  end

  test "should get index" do
    get threes_url
    assert_response :success
  end

  test "should get new" do
    get new_three_url
    assert_response :success
  end

  test "should create three" do
    assert_difference('Three.count') do
      post threes_url, params: { three: { breakfast: @three.breakfast, day: @three.day, dinner: @three.dinner, lunch: @three.lunch } }
    end

    assert_redirected_to three_url(Three.last)
  end

  test "should show three" do
    get three_url(@three)
    assert_response :success
  end

  test "should get edit" do
    get edit_three_url(@three)
    assert_response :success
  end

  test "should update three" do
    patch three_url(@three), params: { three: { breakfast: @three.breakfast, day: @three.day, dinner: @three.dinner, lunch: @three.lunch } }
    assert_redirected_to three_url(@three)
  end

  test "should destroy three" do
    assert_difference('Three.count', -1) do
      delete three_url(@three)
    end

    assert_redirected_to threes_url
  end
end
