require 'test_helper'

class BmitablesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @bmitable = bmitables(:one)
  end

  test "should get index" do
    get bmitables_url
    assert_response :success
  end

  test "should get new" do
    get new_bmitable_url
    assert_response :success
  end

  test "should create bmitable" do
    assert_difference('Bmitable.count') do
      post bmitables_url, params: { bmitable: { bmino: @bmitable.bmino, weightclass: @bmitable.weightclass } }
    end

    assert_redirected_to bmitable_url(Bmitable.last)
  end

  test "should show bmitable" do
    get bmitable_url(@bmitable)
    assert_response :success
  end

  test "should get edit" do
    get edit_bmitable_url(@bmitable)
    assert_response :success
  end

  test "should update bmitable" do
    patch bmitable_url(@bmitable), params: { bmitable: { bmino: @bmitable.bmino, weightclass: @bmitable.weightclass } }
    assert_redirected_to bmitable_url(@bmitable)
  end

  test "should destroy bmitable" do
    assert_difference('Bmitable.count', -1) do
      delete bmitable_url(@bmitable)
    end

    assert_redirected_to bmitables_url
  end
end
