require 'test_helper'

class NeedamountsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @needamount = needamounts(:one)
  end

  test "should get index" do
    get needamounts_url
    assert_response :success
  end

  test "should get new" do
    get new_needamount_url
    assert_response :success
  end

  test "should create needamount" do
    assert_difference('Needamount.count') do
      post needamounts_url, params: { needamount: { age: @needamount.age, height: @needamount.height, total: @needamount.total, weight: @needamount.weight } }
    end

    assert_redirected_to needamount_url(Needamount.last)
  end

  test "should show needamount" do
    get needamount_url(@needamount)
    assert_response :success
  end

  test "should get edit" do
    get edit_needamount_url(@needamount)
    assert_response :success
  end

  test "should update needamount" do
    patch needamount_url(@needamount), params: { needamount: { age: @needamount.age, height: @needamount.height, total: @needamount.total, weight: @needamount.weight } }
    assert_redirected_to needamount_url(@needamount)
  end

  test "should destroy needamount" do
    assert_difference('Needamount.count', -1) do
      delete needamount_url(@needamount)
    end

    assert_redirected_to needamounts_url
  end
end
