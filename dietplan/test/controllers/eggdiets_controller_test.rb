require 'test_helper'

class EggdietsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @eggdiet = eggdiets(:one)
  end

  test "should get index" do
    get eggdiets_url
    assert_response :success
  end

  test "should get new" do
    get new_eggdiet_url
    assert_response :success
  end

  test "should create eggdiet" do
    assert_difference('Eggdiet.count') do
      post eggdiets_url, params: { eggdiet: { breakfast: @eggdiet.breakfast, day: @eggdiet.day, dinner: @eggdiet.dinner, lunch: @eggdiet.lunch } }
    end

    assert_redirected_to eggdiet_url(Eggdiet.last)
  end

  test "should show eggdiet" do
    get eggdiet_url(@eggdiet)
    assert_response :success
  end

  test "should get edit" do
    get edit_eggdiet_url(@eggdiet)
    assert_response :success
  end

  test "should update eggdiet" do
    patch eggdiet_url(@eggdiet), params: { eggdiet: { breakfast: @eggdiet.breakfast, day: @eggdiet.day, dinner: @eggdiet.dinner, lunch: @eggdiet.lunch } }
    assert_redirected_to eggdiet_url(@eggdiet)
  end

  test "should destroy eggdiet" do
    assert_difference('Eggdiet.count', -1) do
      delete eggdiet_url(@eggdiet)
    end

    assert_redirected_to eggdiets_url
  end
end
