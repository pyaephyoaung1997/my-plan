require 'test_helper'

class NormalsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @normal = normals(:one)
  end

  test "should get index" do
    get normals_url
    assert_response :success
  end

  test "should get new" do
    get new_normal_url
    assert_response :success
  end

  test "should create normal" do
    assert_difference('Normal.count') do
      post normals_url, params: { normal: { age: @normal.age, height: @normal.height, total: @normal.total, weight: @normal.weight } }
    end

    assert_redirected_to normal_url(Normal.last)
  end

  test "should show normal" do
    get normal_url(@normal)
    assert_response :success
  end

  test "should get edit" do
    get edit_normal_url(@normal)
    assert_response :success
  end

  test "should update normal" do
    patch normal_url(@normal), params: { normal: { age: @normal.age, height: @normal.height, total: @normal.total, weight: @normal.weight } }
    assert_redirected_to normal_url(@normal)
  end

  test "should destroy normal" do
    assert_difference('Normal.count', -1) do
      delete normal_url(@normal)
    end

    assert_redirected_to normals_url
  end
end
