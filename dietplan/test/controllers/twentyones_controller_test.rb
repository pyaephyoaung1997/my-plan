require 'test_helper'

class TwentyonesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @twentyone = twentyones(:one)
  end

  test "should get index" do
    get twentyones_url
    assert_response :success
  end

  test "should get new" do
    get new_twentyone_url
    assert_response :success
  end

  test "should create twentyone" do
    assert_difference('Twentyone.count') do
      post twentyones_url, params: { twentyone: { breakfast: @twentyone.breakfast, day: @twentyone.day, dinner: @twentyone.dinner, lunch: @twentyone.lunch, morning: @twentyone.morning, night: @twentyone.night, snack: @twentyone.snack, string: @twentyone.string } }
    end

    assert_redirected_to twentyone_url(Twentyone.last)
  end

  test "should show twentyone" do
    get twentyone_url(@twentyone)
    assert_response :success
  end

  test "should get edit" do
    get edit_twentyone_url(@twentyone)
    assert_response :success
  end

  test "should update twentyone" do
    patch twentyone_url(@twentyone), params: { twentyone: { breakfast: @twentyone.breakfast, day: @twentyone.day, dinner: @twentyone.dinner, lunch: @twentyone.lunch, morning: @twentyone.morning, night: @twentyone.night, snack: @twentyone.snack, string: @twentyone.string } }
    assert_redirected_to twentyone_url(@twentyone)
  end

  test "should destroy twentyone" do
    assert_difference('Twentyone.count', -1) do
      delete twentyone_url(@twentyone)
    end

    assert_redirected_to twentyones_url
  end
end
