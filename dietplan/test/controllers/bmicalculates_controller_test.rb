require 'test_helper'

class BmicalculatesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @bmicalculate = bmicalculates(:one)
  end

  test "should get index" do
    get bmicalculates_url
    assert_response :success
  end

  test "should get new" do
    get new_bmicalculate_url
    assert_response :success
  end

  test "should create bmicalculate" do
    assert_difference('Bmicalculate.count') do
      post bmicalculates_url, params: { bmicalculate: { bmi: @bmicalculate.bmi, height: @bmicalculate.height, weight: @bmicalculate.weight } }
    end

    assert_redirected_to bmicalculate_url(Bmicalculate.last)
  end

  test "should show bmicalculate" do
    get bmicalculate_url(@bmicalculate)
    assert_response :success
  end

  test "should get edit" do
    get edit_bmicalculate_url(@bmicalculate)
    assert_response :success
  end

  test "should update bmicalculate" do
    patch bmicalculate_url(@bmicalculate), params: { bmicalculate: { bmi: @bmicalculate.bmi, height: @bmicalculate.height, weight: @bmicalculate.weight } }
    assert_redirected_to bmicalculate_url(@bmicalculate)
  end

  test "should destroy bmicalculate" do
    assert_difference('Bmicalculate.count', -1) do
      delete bmicalculate_url(@bmicalculate)
    end

    assert_redirected_to bmicalculates_url
  end
end
