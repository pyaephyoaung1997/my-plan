require 'test_helper'

class ThirteensControllerTest < ActionDispatch::IntegrationTest
  setup do
    @thirteen = thirteens(:one)
  end

  test "should get index" do
    get thirteens_url
    assert_response :success
  end

  test "should get new" do
    get new_thirteen_url
    assert_response :success
  end

  test "should create thirteen" do
    assert_difference('Thirteen.count') do
      post thirteens_url, params: { thirteen: { breakfast: @thirteen.breakfast, day: @thirteen.day, dinner: @thirteen.dinner, lunch: @thirteen.lunch } }
    end

    assert_redirected_to thirteen_url(Thirteen.last)
  end

  test "should show thirteen" do
    get thirteen_url(@thirteen)
    assert_response :success
  end

  test "should get edit" do
    get edit_thirteen_url(@thirteen)
    assert_response :success
  end

  test "should update thirteen" do
    patch thirteen_url(@thirteen), params: { thirteen: { breakfast: @thirteen.breakfast, day: @thirteen.day, dinner: @thirteen.dinner, lunch: @thirteen.lunch } }
    assert_redirected_to thirteen_url(@thirteen)
  end

  test "should destroy thirteen" do
    assert_difference('Thirteen.count', -1) do
      delete thirteen_url(@thirteen)
    end

    assert_redirected_to thirteens_url
  end
end
