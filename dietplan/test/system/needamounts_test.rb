require "application_system_test_case"

class NeedamountsTest < ApplicationSystemTestCase
  setup do
    @needamount = needamounts(:one)
  end

  test "visiting the index" do
    visit needamounts_url
    assert_selector "h1", text: "Needamounts"
  end

  test "creating a Needamount" do
    visit needamounts_url
    click_on "New Needamount"

    fill_in "Age", with: @needamount.age
    fill_in "Height", with: @needamount.height
    fill_in "Total", with: @needamount.total
    fill_in "Weight", with: @needamount.weight
    click_on "Create Needamount"

    assert_text "Needamount was successfully created"
    click_on "Back"
  end

  test "updating a Needamount" do
    visit needamounts_url
    click_on "Edit", match: :first

    fill_in "Age", with: @needamount.age
    fill_in "Height", with: @needamount.height
    fill_in "Total", with: @needamount.total
    fill_in "Weight", with: @needamount.weight
    click_on "Update Needamount"

    assert_text "Needamount was successfully updated"
    click_on "Back"
  end

  test "destroying a Needamount" do
    visit needamounts_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Needamount was successfully destroyed"
  end
end
