require "application_system_test_case"

class NormalsTest < ApplicationSystemTestCase
  setup do
    @normal = normals(:one)
  end

  test "visiting the index" do
    visit normals_url
    assert_selector "h1", text: "Normals"
  end

  test "creating a Normal" do
    visit normals_url
    click_on "New Normal"

    fill_in "Age", with: @normal.age
    fill_in "Height", with: @normal.height
    fill_in "Total", with: @normal.total
    fill_in "Weight", with: @normal.weight
    click_on "Create Normal"

    assert_text "Normal was successfully created"
    click_on "Back"
  end

  test "updating a Normal" do
    visit normals_url
    click_on "Edit", match: :first

    fill_in "Age", with: @normal.age
    fill_in "Height", with: @normal.height
    fill_in "Total", with: @normal.total
    fill_in "Weight", with: @normal.weight
    click_on "Update Normal"

    assert_text "Normal was successfully updated"
    click_on "Back"
  end

  test "destroying a Normal" do
    visit normals_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Normal was successfully destroyed"
  end
end
