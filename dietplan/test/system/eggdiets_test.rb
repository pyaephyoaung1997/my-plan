require "application_system_test_case"

class EggdietsTest < ApplicationSystemTestCase
  setup do
    @eggdiet = eggdiets(:one)
  end

  test "visiting the index" do
    visit eggdiets_url
    assert_selector "h1", text: "Eggdiets"
  end

  test "creating a Eggdiet" do
    visit eggdiets_url
    click_on "New Eggdiet"

    fill_in "Breakfast", with: @eggdiet.breakfast
    fill_in "Day", with: @eggdiet.day
    fill_in "Dinner", with: @eggdiet.dinner
    fill_in "Lunch", with: @eggdiet.lunch
    click_on "Create Eggdiet"

    assert_text "Eggdiet was successfully created"
    click_on "Back"
  end

  test "updating a Eggdiet" do
    visit eggdiets_url
    click_on "Edit", match: :first

    fill_in "Breakfast", with: @eggdiet.breakfast
    fill_in "Day", with: @eggdiet.day
    fill_in "Dinner", with: @eggdiet.dinner
    fill_in "Lunch", with: @eggdiet.lunch
    click_on "Update Eggdiet"

    assert_text "Eggdiet was successfully updated"
    click_on "Back"
  end

  test "destroying a Eggdiet" do
    visit eggdiets_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Eggdiet was successfully destroyed"
  end
end
