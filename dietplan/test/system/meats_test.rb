require "application_system_test_case"

class MeatsTest < ApplicationSystemTestCase
  setup do
    @meat = meats(:one)
  end

  test "visiting the index" do
    visit meats_url
    assert_selector "h1", text: "Meats"
  end

  test "creating a Meat" do
    visit meats_url
    click_on "New Meat"

    fill_in "Calorie", with: @meat.calorie
    fill_in "Carbohydrate", with: @meat.carbohydrate
    fill_in "Fat", with: @meat.fat
    fill_in "Image", with: @meat.image
    fill_in "Name", with: @meat.name
    fill_in "Protein", with: @meat.protein
    click_on "Create Meat"

    assert_text "Meat was successfully created"
    click_on "Back"
  end

  test "updating a Meat" do
    visit meats_url
    click_on "Edit", match: :first

    fill_in "Calorie", with: @meat.calorie
    fill_in "Carbohydrate", with: @meat.carbohydrate
    fill_in "Fat", with: @meat.fat
    fill_in "Image", with: @meat.image
    fill_in "Name", with: @meat.name
    fill_in "Protein", with: @meat.protein
    click_on "Update Meat"

    assert_text "Meat was successfully updated"
    click_on "Back"
  end

  test "destroying a Meat" do
    visit meats_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Meat was successfully destroyed"
  end
end
