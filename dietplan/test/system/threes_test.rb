require "application_system_test_case"

class ThreesTest < ApplicationSystemTestCase
  setup do
    @three = threes(:one)
  end

  test "visiting the index" do
    visit threes_url
    assert_selector "h1", text: "Threes"
  end

  test "creating a Three" do
    visit threes_url
    click_on "New Three"

    fill_in "Breakfast", with: @three.breakfast
    fill_in "Day", with: @three.day
    fill_in "Dinner", with: @three.dinner
    fill_in "Lunch", with: @three.lunch
    click_on "Create Three"

    assert_text "Three was successfully created"
    click_on "Back"
  end

  test "updating a Three" do
    visit threes_url
    click_on "Edit", match: :first

    fill_in "Breakfast", with: @three.breakfast
    fill_in "Day", with: @three.day
    fill_in "Dinner", with: @three.dinner
    fill_in "Lunch", with: @three.lunch
    click_on "Update Three"

    assert_text "Three was successfully updated"
    click_on "Back"
  end

  test "destroying a Three" do
    visit threes_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Three was successfully destroyed"
  end
end
