require "application_system_test_case"

class BmitablesTest < ApplicationSystemTestCase
  setup do
    @bmitable = bmitables(:one)
  end

  test "visiting the index" do
    visit bmitables_url
    assert_selector "h1", text: "Bmitables"
  end

  test "creating a Bmitable" do
    visit bmitables_url
    click_on "New Bmitable"

    fill_in "Bmino", with: @bmitable.bmino
    fill_in "Weightclass", with: @bmitable.weightclass
    click_on "Create Bmitable"

    assert_text "Bmitable was successfully created"
    click_on "Back"
  end

  test "updating a Bmitable" do
    visit bmitables_url
    click_on "Edit", match: :first

    fill_in "Bmino", with: @bmitable.bmino
    fill_in "Weightclass", with: @bmitable.weightclass
    click_on "Update Bmitable"

    assert_text "Bmitable was successfully updated"
    click_on "Back"
  end

  test "destroying a Bmitable" do
    visit bmitables_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Bmitable was successfully destroyed"
  end
end
