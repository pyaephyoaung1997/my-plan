require "application_system_test_case"

class WomenTest < ApplicationSystemTestCase
  setup do
    @woman = women(:one)
  end

  test "visiting the index" do
    visit women_url
    assert_selector "h1", text: "Women"
  end

  test "creating a Woman" do
    visit women_url
    click_on "New Woman"

    fill_in "Height", with: @woman.height
    fill_in "Mean", with: @woman.mean
    fill_in "Range", with: @woman.range
    click_on "Create Woman"

    assert_text "Woman was successfully created"
    click_on "Back"
  end

  test "updating a Woman" do
    visit women_url
    click_on "Edit", match: :first

    fill_in "Height", with: @woman.height
    fill_in "Mean", with: @woman.mean
    fill_in "Range", with: @woman.range
    click_on "Update Woman"

    assert_text "Woman was successfully updated"
    click_on "Back"
  end

  test "destroying a Woman" do
    visit women_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Woman was successfully destroyed"
  end
end
