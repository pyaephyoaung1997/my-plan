require "application_system_test_case"

class SevensTest < ApplicationSystemTestCase
  setup do
    @seven = sevens(:one)
  end

  test "visiting the index" do
    visit sevens_url
    assert_selector "h1", text: "Sevens"
  end

  test "creating a Seven" do
    visit sevens_url
    click_on "New Seven"

    fill_in "Breakfast", with: @seven.breakfast
    fill_in "Day", with: @seven.day
    fill_in "Dinner", with: @seven.dinner
    fill_in "Lunch", with: @seven.lunch
    click_on "Create Seven"

    assert_text "Seven was successfully created"
    click_on "Back"
  end

  test "updating a Seven" do
    visit sevens_url
    click_on "Edit", match: :first

    fill_in "Breakfast", with: @seven.breakfast
    fill_in "Day", with: @seven.day
    fill_in "Dinner", with: @seven.dinner
    fill_in "Lunch", with: @seven.lunch
    click_on "Update Seven"

    assert_text "Seven was successfully updated"
    click_on "Back"
  end

  test "destroying a Seven" do
    visit sevens_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Seven was successfully destroyed"
  end
end
