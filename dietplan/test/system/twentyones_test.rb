require "application_system_test_case"

class TwentyonesTest < ApplicationSystemTestCase
  setup do
    @twentyone = twentyones(:one)
  end

  test "visiting the index" do
    visit twentyones_url
    assert_selector "h1", text: "Twentyones"
  end

  test "creating a Twentyone" do
    visit twentyones_url
    click_on "New Twentyone"

    fill_in "Breakfast", with: @twentyone.breakfast
    fill_in "Day", with: @twentyone.day
    fill_in "Dinner", with: @twentyone.dinner
    fill_in "Lunch", with: @twentyone.lunch
    fill_in "Morning", with: @twentyone.morning
    fill_in "Night", with: @twentyone.night
    fill_in "Snack", with: @twentyone.snack
    fill_in "String", with: @twentyone.string
    click_on "Create Twentyone"

    assert_text "Twentyone was successfully created"
    click_on "Back"
  end

  test "updating a Twentyone" do
    visit twentyones_url
    click_on "Edit", match: :first

    fill_in "Breakfast", with: @twentyone.breakfast
    fill_in "Day", with: @twentyone.day
    fill_in "Dinner", with: @twentyone.dinner
    fill_in "Lunch", with: @twentyone.lunch
    fill_in "Morning", with: @twentyone.morning
    fill_in "Night", with: @twentyone.night
    fill_in "Snack", with: @twentyone.snack
    fill_in "String", with: @twentyone.string
    click_on "Update Twentyone"

    assert_text "Twentyone was successfully updated"
    click_on "Back"
  end

  test "destroying a Twentyone" do
    visit twentyones_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Twentyone was successfully destroyed"
  end
end
