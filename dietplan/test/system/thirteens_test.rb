require "application_system_test_case"

class ThirteensTest < ApplicationSystemTestCase
  setup do
    @thirteen = thirteens(:one)
  end

  test "visiting the index" do
    visit thirteens_url
    assert_selector "h1", text: "Thirteens"
  end

  test "creating a Thirteen" do
    visit thirteens_url
    click_on "New Thirteen"

    fill_in "Breakfast", with: @thirteen.breakfast
    fill_in "Day", with: @thirteen.day
    fill_in "Dinner", with: @thirteen.dinner
    fill_in "Lunch", with: @thirteen.lunch
    click_on "Create Thirteen"

    assert_text "Thirteen was successfully created"
    click_on "Back"
  end

  test "updating a Thirteen" do
    visit thirteens_url
    click_on "Edit", match: :first

    fill_in "Breakfast", with: @thirteen.breakfast
    fill_in "Day", with: @thirteen.day
    fill_in "Dinner", with: @thirteen.dinner
    fill_in "Lunch", with: @thirteen.lunch
    click_on "Update Thirteen"

    assert_text "Thirteen was successfully updated"
    click_on "Back"
  end

  test "destroying a Thirteen" do
    visit thirteens_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Thirteen was successfully destroyed"
  end
end
