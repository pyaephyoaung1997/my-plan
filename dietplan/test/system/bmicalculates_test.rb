require "application_system_test_case"

class BmicalculatesTest < ApplicationSystemTestCase
  setup do
    @bmicalculate = bmicalculates(:one)
  end

  test "visiting the index" do
    visit bmicalculates_url
    assert_selector "h1", text: "Bmicalculates"
  end

  test "creating a Bmicalculate" do
    visit bmicalculates_url
    click_on "New Bmicalculate"

    fill_in "Bmi", with: @bmicalculate.bmi
    fill_in "Height", with: @bmicalculate.height
    fill_in "Weight", with: @bmicalculate.weight
    click_on "Create Bmicalculate"

    assert_text "Bmicalculate was successfully created"
    click_on "Back"
  end

  test "updating a Bmicalculate" do
    visit bmicalculates_url
    click_on "Edit", match: :first

    fill_in "Bmi", with: @bmicalculate.bmi
    fill_in "Height", with: @bmicalculate.height
    fill_in "Weight", with: @bmicalculate.weight
    click_on "Update Bmicalculate"

    assert_text "Bmicalculate was successfully updated"
    click_on "Back"
  end

  test "destroying a Bmicalculate" do
    visit bmicalculates_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Bmicalculate was successfully destroyed"
  end
end
