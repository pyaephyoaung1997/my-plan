class CreateMeats < ActiveRecord::Migration[5.2]
  def change
    create_table :meats do |t|
      t.string :name
      t.string :calorie
      t.string :carbohydrate
      t.string :protein
      t.string :fat
      t.string :image

      t.timestamps
    end
  end
end
