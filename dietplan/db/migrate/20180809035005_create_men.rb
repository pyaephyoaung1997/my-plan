class CreateMen < ActiveRecord::Migration[5.2]
  def change
    create_table :men do |t|
      t.string :height
      t.string :range
      t.string :mean

      t.timestamps
    end
  end
end
