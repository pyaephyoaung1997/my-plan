class CreateBmitables < ActiveRecord::Migration[5.2]
  def change
    create_table :bmitables do |t|
      t.string :bmino
      t.string :weightclass

      t.timestamps
    end
  end
end
