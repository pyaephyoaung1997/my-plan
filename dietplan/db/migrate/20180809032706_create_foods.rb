class CreateFoods < ActiveRecord::Migration[5.2]
  def change
    add_column :foods, :image, :string
    create_table :foods do |t|
      t.string :name
      t.string :calorie
      t.string :protein
      t.string :carbohydrate
      t.string :fat

      t.timestamps
    end
  end
end
