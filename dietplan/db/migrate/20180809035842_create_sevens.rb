class CreateSevens < ActiveRecord::Migration[5.2]
  def change
    create_table :sevens do |t|
      t.string :day
      t.string :breakfast
      t.string :lunch
      t.string :dinner

      t.timestamps
    end
  end
end
