class AddThemeToNormals < ActiveRecord::Migration[5.2]
  def change
    add_column :normals, :theme, :string
  end
end
