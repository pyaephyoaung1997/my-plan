class CreateNormals < ActiveRecord::Migration[5.2]
  def change
    add_column :normals, :theme, :string
    create_table :normals do |t|
      t.integer :weight
      t.integer :height
      t.integer :age
      t.integer :total
      t.string :theme

      t.timestamps
    end
  end
end
