class CreateBmicalculates < ActiveRecord::Migration[5.2]
  def change
    add_column :bmicalculates, :output, :string
    create_table :bmicalculates do |t|
      t.float :weight
      t.float :height
      t.float :bmi, default: 0

      t.timestamps
    end
  end
end

