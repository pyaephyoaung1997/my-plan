class CreateTwentyones < ActiveRecord::Migration[5.2]
  def change
    create_table :twentyones do |t|
      t.string :day
      t.string :breakfast
      t.string :morning
      t.string :lunch
      t.string :snack
      t.string :dinner
      t.string :night
      

      t.timestamps
    end
  end
end
