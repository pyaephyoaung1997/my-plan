# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_08_20_071032) do

  create_table "abouts", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.integer "record_id", null: false
    t.integer "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "bmicalculates", force: :cascade do |t|
    t.float "weight"
    t.float "height"
    t.integer "bmi"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "output"
  end

  create_table "bmitables", force: :cascade do |t|
    t.string "bmino"
    t.string "weightclass"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "calculates", force: :cascade do |t|
    t.float "weight"
    t.float "height"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "drinks", force: :cascade do |t|
    t.string "name"
    t.string "calorie"
    t.string "carbohydrate"
    t.string "protein"
    t.string "fat"
    t.string "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "eggdiets", force: :cascade do |t|
    t.string "day"
    t.string "breakfast"
    t.string "lunch"
    t.string "dinner"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "foods", force: :cascade do |t|
    t.string "name"
    t.string "calorie"
    t.string "protein"
    t.string "carbohydrate"
    t.string "fat"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "image"
  end

  create_table "fruits", force: :cascade do |t|
    t.string "name"
    t.string "calorie"
    t.string "carbohydrate"
    t.string "protein"
    t.string "fat"
    t.string "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "homes", force: :cascade do |t|
    t.string "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "meats", force: :cascade do |t|
    t.string "name"
    t.string "calorie"
    t.string "carbohydrate"
    t.string "protein"
    t.string "fat"
    t.string "image"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "men", force: :cascade do |t|
    t.string "height"
    t.string "range"
    t.string "mean"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "needs", force: :cascade do |t|
    t.string "weight"
    t.string "height"
    t.integer "age"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "normals", force: :cascade do |t|
    t.integer "weight"
    t.integer "height"
    t.integer "age"
    t.integer "total"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "theme"
  end

  create_table "sevens", force: :cascade do |t|
    t.string "day"
    t.string "breakfast"
    t.string "lunch"
    t.string "dinner"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "snacks", force: :cascade do |t|
    t.string "name"
    t.string "calorie"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "thirteens", force: :cascade do |t|
    t.string "day"
    t.string "breakfast"
    t.string "lunch"
    t.string "dinner"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "threes", force: :cascade do |t|
    t.string "day"
    t.string "breakfast"
    t.string "lunch"
    t.string "dinner"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "twentyones", force: :cascade do |t|
    t.string "day"
    t.string "breakfast"
    t.string "morning"
    t.string "lunch"
    t.string "snack"
    t.string "dinner"
    t.string "night"
    t.string "string"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "women", force: :cascade do |t|
    t.string "height"
    t.string "range"
    t.string "mean"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
