Rails.application.routes.draw do
  resources :abouts
  resources :homes
  resources :snacks
  resources :drinks
  resources :meats
  resources :fruits
  resources :normals
  resources :needamounts
  resources :bmicalculates
  resources :twentyones
  resources :eggdiets
  resources :thirteens
  resources :threes
  resources :sevens
  resources :women
  resources :men
  resources :needs
  resources :bmitables
  resources :calculates
  resources :foods
  
  root 'homes#index'
end
